**Physics programming project**
IMPORTANTE: Per giocare è necessario un gamepad.


Il progetto contiene una scena con un pavimento e uno skateboard.

All'avvio della simulazione vengono spawnati 2 piedi sopra lo skateboard in corrispondenza di due socket.
I piedi sono collegati ai socket tramite degli spring joint.

Utilizzando le levette del gamepad è possibile spostare i socket e di conseguenza i piedi attaccati a essi.
Utilizzando i trigger (L2, R2 / LT, RT) è possibile applicare una forza ai piedi spingendoli in direzione dello skate.
La forza applicata ai piedi è proporzionale alla pressione applicata ai trigger del gamepad.

Ogni oggetto di scena ha il suo physics material con diverse configurazioni.
I physics material non per forza sono pensati per essere realistici ma possono anche essere impostati ad hoc per rendere il gioco più facile al giocatore.

Ad esempio lo skate e i piedi avranno un attrito alto per simulare l'effetto del grip in superficie così che lo skate possa essere controllato in aria trascinando i piedi sul grip.
I piedi avranno una bounciness bassa per evitare di rimbalzare sullo skate e dare maggior controllo al giocatore.

Schema dei comandi (presente anche nel gioco):

- Levetta sinistra: muove il piede sinistro.
- Levetta destra: muove il piede destro.
- Trigger sinistro: applica una forza sul piede sinistro proporzionale alla pressione del trigger (x^3).
- Trigger destro: applica una forza sul piede destro proporzionale alla pressione del trigger (x^3).

- Shoulder sinsitro (L1 / LB): riduce il timescale.
- Shoulder sinsitro (L1 / LB): aumenta il timescale..

- Cerchio / B: ricentra lo skate nella scena.
- Triangolo / Y: attiva/disattiva i comandi a schermo.

Video d'esempio: https://youtu.be/7wkeY0aWj8s

Video d'esempio di simulazione con machine learning (in sviluppo): https://youtu.be/9z2H4X74VFg

Skateboard 3D model purchased by jdthorpe (https://www.cgtrader.com/3d-models/sports/equipment/skateboard-high-quality-realistic-skateboard).
