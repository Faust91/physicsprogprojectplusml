using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skateboard : MonoBehaviour
{
    [Header("Arena")]
    [SerializeField] private Transform arena;

    [Header("Skateboard Setup")]
    [SerializeField] private Transform leftSocket;
    [SerializeField] private Transform rightSocket;

    private Vector3 originalPosition;
    private Quaternion originalRotation;
    private Vector3 leftSocketOriginalPosition;
    private Vector3 rightSocketOriginalPosition;
    private Quaternion leftSocketOriginalRotation;
    private Quaternion rightSocketOriginalRotation;

    public Vector3 OriginalPosition
    {
        get { return originalPosition; }
    }

    public Quaternion OriginalRotation
    {
        get { return originalRotation; }
    }

    [Header("Prefabs")]
    [SerializeField] private GameObject footPrefab;

    [Header("Settings")]
    [SerializeField] private float maxForceAmount = 50000.0f;
    [SerializeField] private float horizontalRoom = 0.2f;
    [SerializeField] private float verticalRoom = 0.1f;

    private GameObject leftFoot;
    private GameObject rightFoot;

    public GameObject LeftFoot
    {
        get { return leftFoot; }
    }

    public Rigidbody LeftFootRB
    {
        get { return leftFootRB; }
    }

    public GameObject RightFoot
    {
        get { return rightFoot; }
    }

    public Rigidbody RightFootRB
    {
        get { return rightFootRB; }
    }

    private Rigidbody leftFootRB;
    private Rigidbody rightFootRB;

    private Foot leftFootScript;
    private Foot rightFootScript;

    [Header("Stats")]
    [SerializeField] [Range(0.0f, 1.0f)] private float leftFootDown = 0.0f;
    [SerializeField] [Range(0.0f, 1.0f)] private float rightFootDown = 0.0f;
    [SerializeField] [Range(-1.0f, 1.0f)] private float leftX = 0.0f;
    [SerializeField] [Range(-1.0f, 1.0f)] private float leftY = 0.0f;
    [SerializeField] [Range(-1.0f, 1.0f)] private float rightX = 0.0f;
    [SerializeField] [Range(-1.0f, 1.0f)] private float rightY = 0.0f;
    [SerializeField] private bool isGrounded = true;

    public bool IsGrounded
    {
        get { return isGrounded; }
    }

    public Vector3 LeftFootPosition
    {
        get { return new Vector3(leftX, leftY, 0.0f); }
        set
        {
            leftX = value.x;
            leftY = value.y;
        }
    }

    public Vector3 RightFootPosition
    {
        get { return new Vector3(rightX, rightY, 0.0f); }
        set
        {
            rightX = value.x;
            rightY = value.y;
        }
    }

    public float LeftFootForce
    {
        get { return leftFootDown; }
        set { leftFootDown = value; }
    }

    public float RightFootForce
    {
        get { return rightFootDown; }
        set { rightFootDown = value; }
    }

    private void Awake()
    {
        originalPosition = transform.localPosition;
        originalRotation = transform.localRotation;
        leftSocketOriginalPosition = leftSocket.localPosition;
        rightSocketOriginalPosition = rightSocket.localPosition;
        leftSocketOriginalRotation = leftSocket.localRotation;
        rightSocketOriginalRotation = rightSocket.localRotation;

        leftFoot = Instantiate(footPrefab, leftSocket.position, Quaternion.identity, arena);
        leftFoot.name = "LeftFoot";
        leftFoot.GetComponent<Foot>().Init(this, FootEnum.LEFT);
        rightFoot = Instantiate(footPrefab, rightSocket.position, Quaternion.identity, arena);
        rightFoot.name = "RightFoot";
        rightFoot.GetComponent<Foot>().Init(this, FootEnum.RIGHT);
        leftFootRB = leftFoot.GetComponent<Rigidbody>();
        rightFootRB = rightFoot.GetComponent<Rigidbody>();
        leftFootScript = leftFoot.GetComponent<Foot>();
        rightFootScript = rightFoot.GetComponent<Foot>();
        SpringJoint leftJoint = leftSocket.GetComponent<SpringJoint>();
        SpringJoint rightJoint = rightSocket.GetComponent<SpringJoint>();
        leftJoint.connectedBody = leftFootRB;
        rightJoint.connectedBody = rightFootRB;
    }

    public void ResetSkateboard()
    {
        transform.localPosition = originalPosition;
        transform.localRotation = originalRotation;
        gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        gameObject.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);
        ResetSockets();
        leftFootScript.ResetFoot(leftSocket);
        rightFootScript.ResetFoot(rightSocket);
        isGrounded = true;
        ResetFeet();
    }

    public void ResetFeet()
    {
        leftX = 0;
        leftY = 0;
        rightX = 0;
        rightY = 0;
        leftFootDown = 0;
        rightFootDown = 0;
    }

    private void ResetSockets()
    {
        leftSocket.localPosition = leftSocketOriginalPosition;
        leftSocket.localRotation = leftSocketOriginalRotation;
        rightSocket.localPosition = rightSocketOriginalPosition;
        rightSocket.localRotation = rightSocketOriginalRotation;
    }

    private void FixedUpdate()
    {
        ResetSockets();

        leftSocket.position += (leftX * horizontalRoom * leftSocket.right);
        leftSocket.position += (leftY * verticalRoom * leftSocket.forward);
        rightSocket.position += (rightX * horizontalRoom * rightSocket.right);
        rightSocket.position += (rightY * verticalRoom * rightSocket.forward);

        leftFootScript.EvaluateForce(leftFootDown * maxForceAmount, leftSocket);
        rightFootScript.EvaluateForce(rightFootDown * maxForceAmount, rightSocket);
    }

    void OnCollisionEnter(Collision theCollision)
    {
        if (theCollision.gameObject.CompareTag("floor"))
        {
            isGrounded = true;
        }
    }

    //consider when character is jumping .. it will exit collision.
    void OnCollisionExit(Collision theCollision)
    {
        if (theCollision.gameObject.CompareTag("floor"))
        {
            isGrounded = false;
        }
    }

}
