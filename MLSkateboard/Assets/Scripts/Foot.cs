using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FootEnum
{
    NONE, LEFT, RIGHT
}

public class Foot : MonoBehaviour
{
    [Header("Appearance")]
    [SerializeField] private Color minColor = Color.green;
    [SerializeField] private Color midColor = Color.yellow;
    [SerializeField] private Color maxColor = Color.red;
    [SerializeField] private Color disabledColor = Color.gray;

    private Rigidbody rb;

    [Header("Stats")]
    private FootEnum footEnum;

    private Material material;
    private Skateboard skateboardSystem;

    private int skateboardLayerMask;
    private int notSkateboardLayerMask;

    public void Init(Skateboard skateboard, FootEnum leftOrRight)
    {
        skateboardSystem = skateboard;
        footEnum = leftOrRight;
    }

    private void Awake()
    {
        material = GetComponent<MeshRenderer>().material;
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        skateboardLayerMask = LayerMask.NameToLayer("Skateboard");
        notSkateboardLayerMask = ~skateboardLayerMask;
    }

    private void Update()
    {
        UpdateFootColor();
    }

    public void EvaluateForce(float amount, Transform socket)
    {
        RaycastHit hit;
        Physics.Raycast(transform.position, -transform.up, out hit, 0.05f, notSkateboardLayerMask);
        if (amount > 0.0f)
        {
            rb.AddForce(-socket.up * amount, ForceMode.Force);
        }
        else
        {
            ResetFoot(socket);
        }
    }

    public void ResetFoot(Transform socket)
    {
        transform.SetPositionAndRotation(socket.position, socket.rotation);
        rb.velocity = new Vector3(0, 0, 0);
        rb.angularVelocity = new Vector3(0, 0, 0);
    }

    private void UpdateFootColor()
    {
        if (footEnum == FootEnum.LEFT)
        {
            SetColor(skateboardSystem.LeftFootForce);
        }
        else if (footEnum == FootEnum.RIGHT)
        {
            SetColor(skateboardSystem.RightFootForce);
        }
    }

    private void SetDisabledColor()
    {
        material.color = disabledColor;
    }

    private void SetColor(float t)
    {
        if (t < 0.5f)
        {
            material.color = Color.Lerp(minColor, midColor, 2.0f * t);
        }
        else
        {
            material.color = Color.Lerp(midColor, maxColor, 2.0f * t - 1.0f);
        }
    }

}
