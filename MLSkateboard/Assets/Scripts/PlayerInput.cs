using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class PlayerInput : MonoBehaviour, SkateboardControls.ISkateboardActions
{
    [SerializeField] private GameObject skateboard;
    [SerializeField] private Text commandSchema;
    private Skateboard skateboardSystem;
    private TimeManager timeManager;

    private void Awake()
    {
        timeManager = FindObjectOfType<TimeManager>();
    }

    private void Start()
    {
        skateboardSystem = skateboard.GetComponent<Skateboard>();
    }

    public void SetSkateboardSystem(Skateboard skateboardSystem)
    {
        this.skateboardSystem = skateboardSystem;
    }

    public Skateboard GetSkateboardSystem()
    {
        return skateboardSystem;
    }

    public void SetLeftFootPosition(Vector2 leftPos)
    {
        skateboardSystem.LeftFootPosition = leftPos;
    }

    public void SetRightFootPosition(Vector2 rightPos)
    {
        skateboardSystem.RightFootPosition = rightPos;
    }

    public void SetLeftForce(float amount)
    {
        skateboardSystem.LeftFootForce = amount * amount * amount;
    }

    public void SetRightForce(float amount)
    {
        skateboardSystem.RightFootForce = amount * amount * amount;
    }

    public void OnLeftFootPush(InputAction.CallbackContext context)
    {
        float amount = context.ReadValue<float>();
        SetLeftForce(amount);
    }

    public void OnRightFootPush(InputAction.CallbackContext context)
    {
        float amount = context.ReadValue<float>();
        SetRightForce(amount);
    }

    public void OnLeftFootMove(InputAction.CallbackContext context)
    {
        Vector2 movement = context.ReadValue<Vector2>();
        SetLeftFootPosition(movement);
    }

    public void OnRightFootMove(InputAction.CallbackContext context)
    {
        Vector2 movement = context.ReadValue<Vector2>();
        SetRightFootPosition(movement);
    }

    public void OnResetPosition(InputAction.CallbackContext context)
    {
        ResetPosition();
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        Jump();
    }

    public void OnHelp(InputAction.CallbackContext context)
    {
        commandSchema.enabled = !commandSchema.enabled;
    }

    public void Jump()
    {
        //skateboardSystem.AIJump();
    }

    public void ResetPosition()
    {
        skateboard.GetComponent<Skateboard>().ResetSkateboard();
    }

    public void OnTimeScaleIncrement(InputAction.CallbackContext context)
    {
        if (context.canceled)
            timeManager.IncrementTimeScale();
    }

    public void OnTimeScaleDecrement(InputAction.CallbackContext context)
    {
        if (context.canceled)
            timeManager.DecrementTimeScale();
    }

}
