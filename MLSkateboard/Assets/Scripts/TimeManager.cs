using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{
    [SerializeField] private float startingTimeScale = 1.0f;
    [SerializeField] private float step = 0.25f;
    [SerializeField] private Text timeScaleText;
    [SerializeField] private string timeScaleTextBefore = "Time scale: ";
    [SerializeField] private string timeScaleTextAfter = "";

    private void Start()
    {
        SetTimeScale(startingTimeScale);
    }

    public void SetTimeScale(float timeScale)
    {
        Time.timeScale = timeScale;
        timeScaleText.text = timeScaleTextBefore + timeScale + timeScaleTextAfter;
    }

    public float GetTimeScale()
    {
        return Time.timeScale;
    }

    public void IncrementTimeScale()
    {
        SetTimeScale(Mathf.Round(Mathf.Clamp01(GetTimeScale() + step) * 100f) / 100.0f);
    }

    public void DecrementTimeScale()
    {
        SetTimeScale(Mathf.Round(Mathf.Clamp01(GetTimeScale() - step) * 100f) / 100.0f);
    }

}
